﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            var matrix = new Matrix();
            Console.WriteLine("Current matrix:");
            matrix.Print();
            matrix.PrintSums();
            matrix.PrintIndexOfEqualRowCol();
            Console.ReadLine();
        }
    }

    class Matrix
    {
        private int rows { get; set; }
        private int cols { get; set; }
        private int[,] mas { get; set; }

        public Matrix()
        {
            mas = new int[,] {
                {1,2,0,4,5,1,7,8},
                {9,7,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0},
                {1,2,0,4,5,4,7,8},
                {1,2,0,4,5,5,7,8},
                {1,0,0,4,5,6,7,8},
                {1,2,0,4,5,7,7,8},
                {1,2,0,4,5,8,7,8}
            };
            rows = cols = mas.GetLength(1);
        }

        public Matrix(int rows, int cols)
        {
            this.rows = rows;
            this.cols = cols;
            mas = new int[rows, cols];
            Random random = new Random();
            for (int i = 0; i < rows; ++i)
                for (int j = 0; j < cols; ++j)
                    mas[i, j] = random.Next(-rows / 2, cols * 2);
        }

        public void Print()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Console.Write(string.Format("{0}\t",mas[i, j]));
                }
                Console.Write("\n");
            }
        }

        public void PrintSums()
        {
            for (int i = 0; i < rows; i++)
            {
                int sum = 0;
                bool chekNegative = false;
                for (int j = 0; j < cols; j++)
                {
                    sum += mas[i, j];
                    if (mas[i, j] < 0 && chekNegative == false) chekNegative = true;
                }
                if (chekNegative) Console.WriteLine("In row №{0} sum = {1}", i, sum);
            }
        }

        public void PrintIndexOfEqualRowCol()
        {
            for (int i = 0; i < rows; ++i)
            {
                bool check = true;
                for (int j = 0; j < rows; ++j)
                {
                    if (j != i && mas[i, j] != mas[j, i])
                    {
                        check = false; break;
                    }
                }
                if (check) Console.WriteLine("k-row = k-col where k = {0}", i);
            }
        }
    }
}
