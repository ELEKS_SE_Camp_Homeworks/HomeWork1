﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows, cols;

            try
            {
                Console.WriteLine("Enter number of rows:");
                rows = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter number of cols:");
                cols = int.Parse(Console.ReadLine());
                var matrix = new Matrix();
                Console.WriteLine("Current matrix:");
                matrix.Print();
                matrix.PrintSums();
                matrix.PrintSaddlePoints();
                Console.ReadLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Wrong input!");
                Console.ReadLine();
            }

        } 
    }

    class Matrix
    {
        private int rows { get; set; }
        private int cols { get; set; }
        private int[,] mas { get; set; }

        public Matrix()
        {
            mas = new int[,] { { 3, 3, -3 }, { 2, 2, 0 }, { 1, 0, -1 } };
            rows = mas.GetLength(0);
            cols = mas.GetLength(1);
        }

        public Matrix(int rows, int cols)
        {
            this.rows = rows;
            this.cols = cols;
            mas = new int[rows, cols];
            Random random = new Random();
            for (int i = 0; i < rows; ++i)
                for (int j = 0; j < cols; ++j)
                    mas[i, j] = random.Next(-rows / 2, cols * 2);
        }

        public void Print()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Console.Write(string.Format("{0}\t", mas[i, j]));
                }
                Console.Write("\n");
            }
        }

        public void PrintSums()
        {
            for (int i = 0; i < rows; i++)
            {
                int sum = 0;
                bool chekNegative = false;
                for (int j = 0; j < cols; j++)
                {
                    sum += mas[i, j];
                    if (mas[i, j] < 0 && chekNegative == false) chekNegative = true;
                }
                if (chekNegative) Console.WriteLine($@"In row №{i} sum = {sum}");
            }
        }

        public void PrintSaddlePoints()
        {
            Console.Write("Saddle points: ");
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    if (GetMinElementOfRow(i) == GetMaxElementOfCol(j))
                        Console.Write("({0},{1}) ", i, j);
                }
            }
        }

        private int GetMaxElementOfCol(int colIndex)
        {
            int max = mas[0, colIndex];
            for (int i = 0; i < mas.GetLength(1); i++)
            {
                if (mas[i, colIndex] > max) max = mas[i, colIndex];
            }
            return max;
        }

        private int GetMinElementOfRow(int rowIndex)
        {
            int min = mas[rowIndex, 0];
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                if (mas[rowIndex, i] < min) min = mas[rowIndex, i];
            }
            return min;
        }

    }
}
