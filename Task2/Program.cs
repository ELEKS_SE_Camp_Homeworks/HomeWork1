﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows, cols;

            try
            {
                Console.WriteLine("Enter number of rows:");
                rows = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter number of cols:");
                cols = int.Parse(Console.ReadLine());
                var matrix = new Matrix(rows,cols);
                Console.WriteLine("Current matrix:");
                matrix.Print();
                Console.WriteLine("Column index without negative elements: {0}", matrix.GetIndexOfPositiveCol());
                Console.WriteLine("Matrix after swaping rows:");
                matrix.ChangeRowsPositions();
                matrix.Print();
                Console.ReadLine();
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Wrong input!");
            }
        }  
    }

    class Matrix
    {
        public int rows { get; private set; }
        public int cols { get; private set; }
        public int[,] mas { get; private set; }

        public Matrix()
        {
            mas = new int[,] { { 3, 3, 3 }, { 2, 2, 0 }, { 1, 0, -1 } };
            rows = mas.GetLength(0);
            cols = mas.GetLength(1);
        }

        public Matrix(int rows, int cols)
        {
            this.rows = rows;
            this.cols = cols;
            mas = new int[rows, cols];
            Random random = new Random();
            for (int i = 0; i < rows; ++i)
                for (int j = 0; j < cols; ++j)
                    mas[i, j] = random.Next(-rows / 2, cols * 2);
        }

        public void Print()
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Console.Write(string.Format("{0}\t", mas[i, j]));
                }
                Console.Write("\n");
            }
        }

        public int GetIndexOfPositiveCol()
        {
            bool isPositive;
            for (int i = 0; i < cols; ++i)
            {
                isPositive = true;
                for (int j = 0; j < rows; ++j)
                {
                    if (mas[j, i] < 0)
                    {
                        isPositive = false; break;
                    }
                }
                if (isPositive) return i;
            }
            return -1;
        }

        public void ChangeRowsPositions()
        {
            int[] equalElements = new int[rows];
            for (int i = 0; i < equalElements.Length; ++i)
            {
                equalElements[i] = 1;
            }
            for (int k = 0; k < rows; ++k)
            {
                for (int i = 0; i < cols; ++i)
                {
                    int count = 1;
                    for (int j = i + 1; j < cols; ++j)
                    {
                        if (mas[k, i] == mas[k, j]) count++;
                    }
                    if (equalElements[k] < count) equalElements[k] = count;
                }
            }
            QuickSort(equalElements, 0, equalElements.Length - 1);
        }

        private void QuickSort(int[] a, int l, int r)
        {
            int x = a[l + (r - l) / 2];
            int i = l;
            int j = r;
            while (i <= j)
            {
                while (a[i] < x) i++;
                while (a[j] > x) j--;
                if (i <= j)
                {
                    SwapRows(i, j);
                    i++;
                    j--;
                }
            }
            if (i < r) QuickSort(a, i, r);
            if (l < j) QuickSort(a, l, j);
        }

        private void SwapRows(int first, int second)
        {
            for (int j = 0; j < cols; j++)
            {
                int temp = mas[first, j];
                mas[first, j] = mas[second, j];
                mas[second, j] = temp;
            }
        }
    }
}
