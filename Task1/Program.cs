﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var library = new Library(3, 5, 9);
            Console.WriteLine(library);
            Console.WriteLine($"Number of books: {library.GetBooksNumber()}");
            Console.WriteLine($"Author with : {library.GetBestLibraryAuthor()}");
            Console.WriteLine($"Book with minimum pages: {library.GetBookWithMinPages()}");
            Console.WriteLine("Section with max books:\n" + library.GetMostFillSection());
            Console.ReadLine();
        }
    }

    class Library : ICountingBooks
    {
        Section[] Sections { get; set; }

        public Library(int section1, int section2, int section3)
        {
            Sections = new Section[3];
            FillUpSections(section1, section2, section3);
        }

        private void FillUpSections(int booksCount1, int booksCount2, int booksCount3)
        {
            Sections[0] = new IT(booksCount1);
            Sections[1] = new UkrLit(booksCount2);
            Sections[2] = new ForeignLit(booksCount3);
        }

        public int GetBooksNumber()
        {
            return Sections[0].GetBooksNumber() + Sections[1].GetBooksNumber() + Sections[2].GetBooksNumber();
        }

        public Section GetMostFillSection()
        {
            int index = 0;
            for (int i = 0; i < Sections.Length; i++)
            {
                if (Sections[index].CompareTo(Sections[i]) < 0) index = i;
            }
            return Sections[index];
        }

        public Book GetBookWithMinPages()
        {
            int index = 0;
            for (int i = 0; i < Sections.Length; i++)
            {
                if (Sections[index].GetBookWithMinPages().CompareTo(Sections[i].GetBookWithMinPages()) > 0) index = i;
            }
            return Sections[index].GetBookWithMinPages();
        }

        public Author GetBestLibraryAuthor()
        {
            int index = 0;
            for (int i = 0; i < Sections.Length; i++)
            {
                if (Sections[index].GetBestSectionAuthor().CompareTo(Sections[i].GetBestSectionAuthor()) < 0) index = i;
            }
            return Sections[index].GetBestSectionAuthor();
        }

        public override string ToString()
        {
            StringBuilder booksList = new StringBuilder();
            foreach (var item in Sections)
            {
                booksList.Append(item).Append("\n");
            }

            return "Library:\n" + booksList.ToString();
        }
    }

    class IT : Section
    {
        public IT(int booksCount)
        {
            Books = new Book[booksCount];
            GenerateITBooks();
        }

        private void GenerateITBooks()
        {
            Random rnd = new Random();
            for (int i = 0; i < Books.Length; i++)
            {
                int publishesNum = rnd.Next(2, 40);
                int pagesNum = rnd.Next(200, 500);
                Books[i] = new Book(new Author($"firstname{i}", $"lastname{i}", publishesNum), pagesNum);
            }
        }

        public override string ToString()
        {
            return "This is IT section:\n" + base.ToString();
        }
    }

    class UkrLit : Section
    {
        public UkrLit(int booksCount)
        {
            Books = new Book[booksCount];
            GenerateUkrLitBooks();
        }

        private void GenerateUkrLitBooks()
        {
            Random rnd = new Random();
            for (int i = 0; i < Books.Length; i++)
            {
                int publishesNum = rnd.Next(2, 40);
                int pagesNum = rnd.Next(90, 300);
                Books[i] = new Book(new Author($"firstname{i}", $"lastname{i}", publishesNum), pagesNum);
            }
        }

        public override string ToString()
        {
            return "This is Ukrainian Literature section:\n" + base.ToString();
        }
    }

    class ForeignLit : Section
    {
        public ForeignLit(int booksCount)
        {
            Books = new Book[booksCount];
            GeneratForeignLiteBooks();
        }

        public override string ToString()
        {
            return "This is Foreign Literature section:\n" + base.ToString();
        }

        private void GeneratForeignLiteBooks()
        {
            Random rnd = new Random();
            for (int i = 0; i < Books.Length; i++)
            {
                int publishesNum = rnd.Next(2, 40);
                int pagesNum = rnd.Next(80, 320);
                Books[i] = new Book(new Author($"firstname{i}",$"lastname{i}", publishesNum), pagesNum);
            }
        }
    }

    abstract class Section : ICountingBooks, IComparable
    {
        public Book[] Books { get; set; }

        public int CompareTo(object section)
        {
            return this.Books.Length - ((Section)section).GetBooksNumber();
        }

        public int GetBooksNumber()
        {
            return Books.Length;
        }

        public Author GetBestSectionAuthor()
        {
            int index = 0;
            for (int i = 0; i < Books.Length; i++)
            {
                if (Books[index].CompareAuthors(Books[i].Author) < 0) index = i;
            }

            return Books[index].Author;
        }

        public Book GetBookWithMinPages()
        {
            int index = 0;
            for (int i = 0; i < Books.Length; i++)
            {
                if (Books[index].CompareTo(Books[i]) > 0) index = i;
            }

            return Books[index];
        }

        public override string ToString()
        {
            StringBuilder booksList = new StringBuilder();
            foreach (var item in Books)
            {
                booksList.Append(item).Append("\n");
            }

            return booksList.ToString();
        }
    }

    class Book : IComparable
    {
        public Author Author { get; private set; }
        public int NumberOfPages { get; private set; }

        public Book(Author author, int numberOfPages)
        {
            this.Author = author;
            this.NumberOfPages = numberOfPages;
        }

        public override string ToString()
        {
            return $"Author: {Author}, number of pages: {NumberOfPages}";
        }

        public int CompareAuthors(Object obj)
        {
            return Author.CompareTo(obj);
        }

        public int CompareTo(object book)
        {
            return this.NumberOfPages - ((Book)book).NumberOfPages;
        }
    }

    abstract class Human
    {
        protected string FirstName { get; set; }
        protected string LastName { get; set; }

        public Human(string firstName,string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
    class Author : Human,ICountingBooks,IComparable
    {
        Book[] Books { get; set; }
       
        public Author(string name,string surname,int booksCount):base(name,surname)
        {
            Books = new Book[booksCount];
            GenerateAuthtorBooks();
        }

        private void GenerateAuthtorBooks()
        {
            Random rnd = new Random();
            for (int i = 0; i < Books.Length; i++)
            {
                int pagesNum = rnd.Next(200, 500);
                Books[i] = new Book(this, pagesNum);
            }
        }

        public int GetBooksNumber()
        {
            return Books.Length;
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}(books:{Books.Length})";
        }

        public int CompareTo(object author)
        {
            return this.Books.Length - ((Author)author).GetBooksNumber();
        }
    }

    interface ICountingBooks
    {
        int GetBooksNumber();
    }
}
